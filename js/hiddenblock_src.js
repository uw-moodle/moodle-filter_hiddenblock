
/** Unminified js for what is put in the "on-click" section of the show link.
 * Modify it here, re-minify, and insert into the onclick attribute.
 * This file is never actually called in the filter
**/
var id = '';
var htext = document.getElementById(id + '_header_text');
var himg = document.getElementById(id + '_header_img');
var content = document.getElementById(id + '_content');
if(content.className.match(/\bcontent-hidden\b/)){
    htext.innerHTML = 'Hide';
    himg.className = himg.className.replace(/\bplus-image\b/, 'minus-image');
    content.className = 'content-visible';
}else{
    htext.innerHTML = 'Show';
    himg.className = himg.className.replace(/\bminus-image\b/, 'plus-image');
    content.className = 'content-hidden';
}