<?php // $id$
/// Hidden block filter

defined('MOODLE_INTERNAL') || die();

// Given XML multilinguage text, return relevant text according to
// current language:
//   - look for multilang blocks in the text.
//   - if there exists texts in the currently active language, print them.
//   - else, if there exists texts in the current parent language, print them.
//   - else, print the first language in the text.
// Please note that English texts are not used as default anymore!
//
// This version is based on original multilang filter by Gaetan Frenoy,
// rewritten by Eloy and skodak.
//
// Following new syntax is not compatible with old one:
//   <span lang="XX" class="multilang">one lang</span><span lang="YY" class="multilang">another language</span>

class filter_hiddenblock extends moodle_text_filter {
    function filter($text, array $options = array()) {
        global $CFG, $PAGE;
        static $count = 0;

        // If no hidden blocks, just skip the filter
        if (!preg_match('/<div([^>]*class="hiddenblock"[^>]*)>/i',$text)) {
            return $text;
        }

        $sections = array();
        $offset = 0;
        while (preg_match('#<div[^>]*class="hiddenblock"[^>]*>#si', $text, $match,  PREG_OFFSET_CAPTURE, $offset)) {
            $this_offset = $match[0][1];

            // Recursive regexp to find the extent of the <div>
            if (preg_match('#<div[^>]*>(?>(?:(?!</?div).)+|(?R))*</div>#si', $text, $matches2, PREG_OFFSET_CAPTURE, $this_offset)) {
                array_push($sections, array($matches2[0][0], $matches2[0][1]));
                $offset = $this_offset + strlen($matches2[0][0]);
            }
        }

        $newtext = $text;
        while ( count($sections) > 0 ) {
            list ($str, $this_offset) = array_pop($sections);

            preg_match('#^<div([^>]*)>(.*)</div>$#is', $str, $match);
            list (,$attrs, $content) = $match;
            if (!preg_match('/hiddenblock/', $attrs)) {
                // must have been an error parsing the block
                continue;
            }
            preg_match('/title="(.*?)"/is', $attrs, $m);
            $desc = empty($m[1]) ? get_string('defaulttitle', 'filter_hiddenblock') : $m[1];

            $id = 'filter_hideblock_' . uniqid(); //we need something unique because it might be stored in text cache

            $js = 'var id="' . $id . '",htext=document.getElementById(id+"_header_text"),himg=document.getElementById(id+"_header_img"),content=document.getElementById(id+"_content");content.className.match(/\bcontent-hidden\b/)?(htext.innerHTML="Hide",himg.className=himg.className.replace(/\bplus-image\b/,"minus-image"),content.className="content-visible"):(htext.innerHTML="Show",himg.className=himg.className.replace(/\bminus-image\b/,"plus-image"),content.className="content-hidden");';

            $html = '
                <div id="'.$id.'" class="filter_hideblock">
                 <div class="header" id="' . $id . '_header">
                   <a href="javascript:void(0)" onclick="' . htmlspecialchars($js) . '">
                        <div id="' . $id . '_header_img" class="hb_inline hide-show-image plus-image">
                        </div>
                        <div id="' . $id . '_header_text" class="hb_inline">
                         '.$desc.'
                        </div>
                   </a>
                 </div>
                 <div id="' . $id . '_content" class="content-hidden">'.$content.'</div>
                </div>';

            $newtext = (($this_offset > 0)? substr($newtext,0,$this_offset) : '').$html.substr($newtext,$this_offset + strlen($str));
        }

        return $newtext;
    }
}
