This is a very simple Text Filter that allows blocks to be hidden by default
and then shown when a student clicks on a control. It works by searching text
for <div class="hiddenblock"> elements which are then instrumented with javascript
code to create a hide/show control for the block.  One use of the filter would be to
hide an answer in a document until the reader has had time to think about a question.

This filter works alongside a tinymce plugin to simplify creation of these div's.


Installation:

1.  Copy the contents of this directory to filter/hiddenblock

2.  Copy the tinymce editor plugin to lib/editor/tinymce/plugins/hiddenblock

3.  Enable the hiddenblock filter from within moodle.