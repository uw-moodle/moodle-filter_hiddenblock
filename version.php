<?php
defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014060400;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2012120300;        // Requires this Moodle version
$plugin->component = 'filter_hiddenblock';  // Full name of the plugin (used for diagnostics)
